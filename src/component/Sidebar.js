import React from 'react';
import './Sidebar.css';
import UniversityAppLogo from './images/UniversityAppLogo.png';
import University from './images/University.png'; 
import { VscHome } from "react-icons/vsc";
import { AiOutlineUser} from "react-icons/ai";
import { VscMortarBoard } from "react-icons/vsc";
import { IoSettingsOutline, IoHelpCircleOutline } from "react-icons/io5";
import {Link} from 'react-router-dom'; 

function Sidebar () {
    // const [car,setCar] = useState (false);
    // const [car1,setCar1] = useState (false);

    // const Activemenu = (e) => {
    //     if (e.target.innerText === "Dashboard"){
    //         setCar(true);
    //         setCar1(false);
    //     }
        
    //     if(e.target.innerText === "My University")
    //     {
    //         console.log(car1 );
    //         setCar(false);
    //         setCar1(true);
            
    //     }
        
    // };

    // var activeButton = car ? "active-btn" : null ;
    // var activeButton1 = car1 ? "active-btn" : null ;

    return (
        <div className='navbar-container'>
            <div className='sub-navbar'>
                <div className='logo-container'>
                    <div><img src = {UniversityAppLogo} alt = 'logo' className='App-logo1' />
                    <img src = {University} alt = 'logo-text' className='logo-text' /></div>
                </div>
                <div className='menu-container'>
                    <div className= "dashboard-btn" >
                        <VscHome className='vscHome' />
                        <p> Dashboard </p>
                    </div>
                    <div className='student-btn'>
                        <AiOutlineUser className='AiOutlineUse' />
                        <p> Students </p>
                    </div>
                    <div className='sub-click'>
    
                            <div className='university-btn'>
                                <VscMortarBoard className='VscMortarBoard' /> 
                                <p>My University</p>
                            </div>
                
                    </div>
                    
                    <div  className='settings-btn'>
                        <IoSettingsOutline className='IoSettingsOutline' />
                        <p> Settings </p>
                    </div>
                    <div  className='help-btn'>
                        <IoHelpCircleOutline className='IoHelpCircleOutline' />
                        <p> Help </p>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Sidebar;