import React from 'react';
import './Dashboard.css';
import { FiSearch } from "react-icons/fi";
import {IoIosNotificationsOutline} from "react-icons/io";
import UniversityBadge from './images/UniversityBadge.png';
import Criclegraph from './images/Criclegraph.png';
import Saly from './images/Saly-14.png';
import { BsThreeDotsVertical } from "react-icons/bs";
import Card from './images/Card.png';

function Dashboard () {
    return (
        <>
            <div className='row'>
                <div className='col-md-12'>
                    <div className='header-icons'>
                        < FiSearch className='FiSearch' />
                        < IoIosNotificationsOutline className='IoIosNotificationsOutline' />
                        <div className='badge'><img src = {UniversityBadge} alt='badge' /></div>
                    </div>
                </div>
            </div>
            <div className='row'>
                <p className='good-text'> Good Morning </p>
            </div> 
            <div className='row'>
                <div className='col-md-4'>
                    <div className='student'>
                        <p className='num'> 12800 </p> 
                        <p className='txt' > student </p>
                    </div>
                </div>
                <div className='col-md-4'>
                    <div className='student'>
                        <p className='num'> 28 </p>
                        <p className='txt'> student </p>
                    </div>
                </div>
                    <div className='col-md-4'>
                        <div className='student'>
                            <p className='num'> 920 </p>
                            <p className='txt'> student </p>
                        </div>
                </div>
            </div> 
            <div className='row'>
                <div className='col-md-4'>
                    <div className='Assessment-result'>
                        <div><p className='Assessment-heading'> Assessment results on average </p></div>
                        <div className='Average-container'>
                            <img src = {Criclegraph} alt='circle-graph' className='circle3'/>
                        </div>
                        <div className='unordered-list'>
                            <div className='idea'><p> Ideas and opportunities </p></div>
                            <div className='resources'><p> Resources </p></div>
                            <div className='into'><p> Into action </p></div>
                        </div>
                        <div className='saly'>
                            <img src = {Saly} alt='cartoon'/>
                        </div>
                    </div>
                </div>


                <div className='col-md-8'>
                    <div className='Comparative-Bar'>
                        <div className='top-text-container'>
                            <p> Comparative Bar </p>
                            <BsThreeDotsVertical  className='BsThreeDotsVertical' />
                        </div>
                        <div className='progressBar-container'>
                                <div className='Comparative-sub-bar'>
                                    <div className='sub-English-Proficiency'>
                                        <div className='sub-English-Proficiency1'>
                                            <p> 43% </p>
                                            <p> English Proficiency </p>
                                            <p> 57% </p>
                                        </div>
                                        <div id="myProgress">
                                            <div id="myBar1"></div>
                                            <div id="myBar2" ></div>
                                        </div>
                                        <div className='sub-English-Proficiency2'>
                                            <p> Planned </p>
                                            <p className='Not'> Not Planned </p>
                                        </div>
                                    </div>

                                    <div className='IT-Skills'>
                                        <div className='IT-Skills1'>
                                            <p> 19% </p>
                                            <p> IT Skills </p>
                                            <p> 81% </p>
                                        </div>
                                        <div id="myProgress1">
                                            <div id="myBar3"></div>
                                            <div id="myBar4" ></div>
                                        </div>
                                        <div className='IT-Skills2'>
                                            <p> Planned </p>
                                            <p className='Not'> Not Planned </p>
                                        </div>
                                    </div>



                                    <div className='Employability-Factors'>
                                        <div className='Employability-Factors1'>
                                            <p> 68% </p>
                                            <p> Employability Factors </p>
                                            <p> 32% </p>
                                        </div>
                                        <div id="myProgress2">
                                            <div id="myBar5"></div>
                                            <div id="myBar6" ></div>
                                        </div>
                                        <div className='Employability-Factors2'>
                                            <p> Planned </p>
                                            <p className='Not'> Not Planned </p>
                                        </div>
                                    </div>

                                    <div className='Other-Factors'>
                                        <div className='Other-Factors1'>
                                            <p> 94% </p>
                                            <p> Employability Factors </p>
                                            <p> 6% </p>
                                        </div>
                                        <div id="myProgress3">
                                            <div id="myBar7"></div>
                                            <div id="myBar8" ></div>
                                        </div>
                                        <div className='Other-Factors2'>
                                            <p> Planned </p>
                                            <p className='Not'> Not Planned </p>
                                        </div>
                                    </div>


                                </div>
                        </div>
                    </div>
                    <img src= {Card} alt='graph' className='graph-card' />
                </div>
            </div>  
            
        </>
    );
}

export default Dashboard;