import React from 'react';
import './App.css';
import Sidebar from './component/Sidebar';
import Dashboard from './component/Dashboard';

function App() {
  return (
    <div className="App">
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-3'>
          <Sidebar/> 
          </div>
          <div className='col-md-9'> 
           <Dashboard />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
